function Student(data) {
    this.name = ko.observable(data.name);
    this.lastname = ko.observable(data.lastname);
    this.isDone = ko.observable(data.isDone);
}

function StudentListViewModel() {
    // Data
    var self = this;
    self.students = ko.observableArray([]);
    self.newName = ko.observable();
    self.newLastName = ko.observable();
    self.totalStudents = ko.computed(function() {
        return ko.utils.arrayFilter(self.students(), function(student) { return !student.isDone() });
    });

    // Operations
    self.addStudent = function() {
        self.students.push(new Student({ name: this.newName(), lastname: this.newLastName() }));
        self.newName("");
        self.newLastName("");
    };
    self.removeStudent = function(student) { self.students.remove(student) };
}

ko.applyBindings(new StudentListViewModel());